﻿using CRUD.Data;
using CRUD.Models;
using CRUD.Services.Contracts;

namespace CRUD.Services.Repositories
{
    public class PostServices : ServiceBase, IPostServices
    {
        public PostServices(AuthDbContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
        {
        }

        public IQueryable<PostViewModel> GetUserPosts()
        {
            IQueryable<PostViewModel> posts = this.DbContext.Posts.Where(post => post.AuthorUserId == this.UserId);
            return posts;
        }

        public PostViewModel GetPostDetails(string Slug)
        {
            PostViewModel targetPost = this.DbContext.Posts.Where(post => post.Slug == Slug).First();
            return targetPost;
        }

        public void CreateNewPost(PostViewModel post)
        {
            post.AuthorUserId = this.UserId;

            this.DbContext.Posts.Add(post);
            this.Commit();
        }

        public void EditPost(PostViewModel newPost)
        {
            newPost.AuthorUserId = this.UserId;
            PostViewModel targetPost = this.DbContext.Posts.Where(post => post.Slug == newPost.Slug).First();
            if (targetPost != null)
            {
                this.DbContext.Entry(targetPost).CurrentValues.SetValues(newPost);
                this.Commit();
            }
        }

        public void DeletePost(string Slug)
        {
            PostViewModel targetPost = this.DbContext.Posts.Where(post => post.Slug == Slug).First();
            if (targetPost != null)
            {
                this.DbContext.Posts.Remove(targetPost);
                this.Commit();
            }
        }
    }
}
