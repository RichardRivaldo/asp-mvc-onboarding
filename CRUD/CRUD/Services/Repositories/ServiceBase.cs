﻿using CRUD.Data;
using CRUD.Services.Contracts;
using Microsoft.AspNetCore.Components;
using System.Security.Claims;

namespace CRUD.Services.Repositories
{
    public class ServiceBase : IServiceBase
    {
        public AuthDbContext DbContext { get; set; }
        public string UserId { get; set; }

        public ServiceBase(AuthDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            this.DbContext = context;
            var httpContext = httpContextAccessor.HttpContext;
            if (httpContext != null)
            {
                this.UserId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }
        }

        public void Commit()
        {
            this.DbContext.SaveChanges();
        }

        public string GetCurrentUser()
        {
            return this.UserId;
        }
    }
}
