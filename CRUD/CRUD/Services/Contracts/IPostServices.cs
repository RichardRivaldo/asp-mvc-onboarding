﻿using CRUD.Models;

namespace CRUD.Services.Contracts
{
    public interface IPostServices : IServiceBase
    {
        IQueryable<PostViewModel> GetUserPosts();
        PostViewModel GetPostDetails(string Slug);
        void CreateNewPost(PostViewModel post);
        void EditPost(PostViewModel newPost);
        void DeletePost(string Slug);
    }
}
