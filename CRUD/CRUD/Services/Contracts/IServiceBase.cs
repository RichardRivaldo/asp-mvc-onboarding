﻿using CRUD.Data;

namespace CRUD.Services.Contracts
{
    public interface IServiceBase
    {
        AuthDbContext DbContext { get; set; }
        string UserId { get; set; }

        void Commit();

        string GetCurrentUser();
    }
}
