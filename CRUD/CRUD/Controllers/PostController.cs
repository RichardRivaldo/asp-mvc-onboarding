﻿using CRUD.Areas.Identity.Data;
using CRUD.Data;
using CRUD.Models;
using CRUD.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace CRUD.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        public IPostServices postServices;

        public PostController(IPostServices postServices)
        {
            this.postServices = postServices;
        }
        public IActionResult Index()
        {
            var posts = this.postServices.GetUserPosts();
            return View(posts);
        }

        public IActionResult Details(string Slug)
        {
            PostViewModel targetPost = this.postServices.GetPostDetails(Slug);
            return View(targetPost);
        }

        [HttpGet]
        public IActionResult Create()
        {
            PostViewModel newPost = new PostViewModel();
            newPost.AuthorUserId = this.postServices.GetCurrentUser();
            return View(newPost);
        }

        [HttpPost]
        public IActionResult Create(PostViewModel post)
        {
            if (ModelState.IsValid)
            {
                this.postServices.CreateNewPost(post);
                return RedirectToAction("Details", new { Slug = post.Slug });
            }
            else
            {
                post.AuthorUserId = this.postServices.GetCurrentUser();
                return View(post);
            }
        }

        [HttpGet]
        public IActionResult Edit(string Slug)
        {
            PostViewModel targetPost = this.postServices.GetPostDetails(Slug);
            return View(targetPost);
        }

        [HttpPost]
        public IActionResult Edit(PostViewModel newPost)
        {
            if (ModelState.IsValid)
            {
                this.postServices.EditPost(newPost);
                return RedirectToAction("Details", new { Slug = newPost.Slug });
            }
            else
            {
                return View(newPost);
            }
        }

        public IActionResult Delete(string Slug)
        {
            this.postServices.DeletePost(Slug);
            return RedirectToAction("Index");
        }
    }
}
