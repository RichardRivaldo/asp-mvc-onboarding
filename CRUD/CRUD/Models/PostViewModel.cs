﻿using System.ComponentModel.DataAnnotations;

namespace CRUD.Models
{
    public class PostViewModel
    {
        [Required(ErrorMessage = "Please enter the title of your post!")]
        public string Title { get; set; }
        [Key]
        [Required(ErrorMessage = "Please enter the slug of your post!")]
        [RegularExpression("^[a-z]+(-[a-z]+)*$", ErrorMessage = "Please enter valid slug! Lowercase letters and hyphens only!")]
        public string Slug { get; set; }

        [Required(ErrorMessage = "Please enter the content of your post!")]
        public string Content { get; set; }
        public string AuthorUserId { get; set; }
    }
}
